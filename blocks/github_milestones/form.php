<?php

defined('C5_EXECUTE') or die("Access Denied.");

?>

<div class="form-group">
    <label class="control-label" for="repository"><?=t('Repository')?></label>
    <input class="form-control" name="repository" value="<?= h($repository) ?>" />
</div>