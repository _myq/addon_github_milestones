<?php
namespace Concrete\Package\GithubMilestones\Block\GithubMilestones;
use \Concrete\Core\Block\BlockController;

class Milestone
{

    protected $publicUrl;

    protected $labelsUrl;

    protected $title;

    protected $description;

    protected $dueOn;

    protected $openIssues;

    protected $closedIssues;

    /**
     * @return mixed
     */
    public function getPublicUrl()
    {
        return $this->publicUrl;
    }

    /**
     * @param mixed $publicUrl
     */
    public function setPublicUrl($publicUrl)
    {
        $this->publicUrl = $publicUrl;
    }

    /**
     * @return mixed
     */
    public function getLabelsUrl()
    {
        return $this->labelsUrl;
    }

    /**
     * @param mixed $labelsUrl
     */
    public function setLabelsUrl($labelsUrl)
    {
        $this->labelsUrl = $labelsUrl;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDueOn()
    {
        return $this->dueOn;
    }

    /**
     * @param mixed $dueOn
     */
    public function setDueOn($dueOn)
    {
        $this->dueOn = $dueOn;
    }

    /**
     * @return mixed
     */
    public function getOpenIssues()
    {
        return $this->openIssues;
    }

    /**
     * @param mixed $openIssues
     */
    public function setOpenIssues($openIssues)
    {
        $this->openIssues = $openIssues;
    }

    /**
     * @return mixed
     */
    public function getClosedIssues()
    {
        return $this->closedIssues;
    }

    /**
     * @param mixed $closedIssues
     */
    public function setClosedIssues($closedIssues)
    {
        $this->closedIssues = $closedIssues;
    }

    public function getProgress()
    {
        $total = $this->getOpenIssues() + $this->getClosedIssues();
        $progress = $this->getClosedIssues() / $total;
        return round($progress * 100);
    }


}

class Controller extends BlockController
{

    protected $btTable = 'btGitHubMilestones';
    protected $btInterfaceWidth = "400";
    protected $btInterfaceHeight = "240";
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 3600;
    protected $btDefaultSet = 'social';
    protected $btIgnorePageThemeGridFrameworkContainer = true;

    public function getBlockTypeDescription()
    {
        return t("Displays a list and status of a GitHub project's milestones.");
    }

    public function getBlockTypeName()
    {
        return t("GitHub Milestones");
    }

    public function view()
    {
        $client = $this->app->make('http/client');
        $uri = sprintf('https://api.github.com/repos/%s/milestones', $this->repository);
        $request = $client->getRequest();
        $request->setUri($uri);
        $response = $client->send();
        $json = json_decode($response->getBody());

        $milestones = [];
        if ($json) {
            $milestones = $this->createFromJson($json);
        }
        $this->set('milestones', $milestones);
    }

    protected function createFromJson($json)
    {
        $milestones = [];
        foreach($json as $milestone) {
            $m = new Milestone();
            $m->setTitle($milestone->title);
            $m->setDescription($milestone->description);
            $m->setPublicUrl($milestone->html_url);
            $m->setLabelsUrl($milestone->labels_url);
            if ($milestone->due_on) {
                $dueOn = new \DateTime($milestone->due_on);
                $m->setDueOn($dueOn);
            }
            $m->setOpenIssues($milestone->open_issues);
            $m->setClosedIssues($milestone->closed_issues);
            $milestones[] = $m;
        }

        return $milestones;
    }


}